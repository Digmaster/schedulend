#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'schedulend'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET latin1") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to feednd DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#OrderItems
dropTableQuery = ("DROP TABLE IF EXISTS Classes_Taken")
cursor.execute(dropTableQuery)

#Orders
dropTableQuery = ("DROP TABLE IF EXISTS Schedules")
cursor.execute(dropTableQuery)

#Users
dropTableQuery = ("DROP TABLE IF EXISTS Users")
cursor.execute(dropTableQuery)

#Hours
dropTableQuery = ("DROP TABLE IF EXISTS Classes")
cursor.execute(dropTableQuery)

########################
## Create tables next ##
########################


#ClassDB
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Classes` (
						`Class_ID` INT NOT NULL AUTO_INCREMENT,
						`College` VARCHAR(4) NULL,
						`Course` VARCHAR(12) NULL,
						`Section` VARCHAR(3) NULL,
						`Title` VARCHAR(100) NULL,
						`Credits` INT NULL,
						`Max` INT NULL,
						`CRN` CHAR(5) NULL,
						`Instructor` TEXT NULL,
						`Start` TIME NULL,
						`End` TIME NULL,
						`Days` VARCHAR(7) NULL,
						`Where` VARCHAR(100) NULL,
						PRIMARY KEY (`Class_ID`))
						'''
                    )
cursor.execute(createTableQuery)

#Users
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Users` (
						`User_ID` INT NOT NULL AUTO_INCREMENT,
						`Email` VARCHAR(100),
						`Password` VARCHAR(100),
						PRIMARY KEY (`User_ID`))
						'''
                    )
cursor.execute(createTableQuery)

#Schedules
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Schedules` (
						`Schedule_ID` INT NOT NULL AUTO_INCREMENT,
						`User_ID` INT NOT NULL,
						`Class_ID` INT NOT NULL,
						PRIMARY KEY (`Schedule_ID`),
						CONSTRAINT `User_ID_Schedules`
						FOREIGN KEY (`User_ID`)
						REFERENCES `Users` (`User_ID`)
						ON DELETE CASCADE,
						CONSTRAINT `Class_ID_Schedules`
						FOREIGN KEY (`Class_ID`)
						REFERENCES `Classes` (`Class_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Classes_Taken
createTableQuery = ('''CREATE TABLE IF NOT EXISTS `Classes_Taken` (
						`Classes_Taken_ID` INT NOT NULL AUTO_INCREMENT,
						`User_ID` INT NOT NULL,
						`Class_ID` INT NOT NULL,
						PRIMARY KEY (`Classes_Taken_ID`),
						CONSTRAINT `User_ID_Taken`
						FOREIGN KEY (`User_ID`)
						REFERENCES `Users` (`User_ID`)
						ON DELETE CASCADE,
						CONSTRAINT `Class_ID_Taken`
						FOREIGN KEY (`Class_ID`)
						REFERENCES `Classes` (`Class_ID`)
						ON DELETE CASCADE)
						'''
                    )
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()